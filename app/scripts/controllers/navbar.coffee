'use strict'

angular.module('giffyApp')
	.controller 'NavbarCtrl', ($scope, $location, $rootScope) ->
	  # $scope.menu = [
	  #   title: 'Home'
	  #   link: '/'
	  # ,
	  #   title: 'About'
	  #   link: '#'
	  # ,
	  #   title: 'Contact'
	  #   link: '#'
	  # ]
	  $scope.isActive = (route) ->
	    route is $location.path()
	  $scope.save = ()->
	  	$rootScope.$broadcast 'save-gif'
	  $rootScope.$on 'saved-gif', (status, data)->
	  	console.log 'NAV CTRL', data
	  	$scope.gif = data.gif