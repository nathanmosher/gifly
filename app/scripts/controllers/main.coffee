'use strict'

angular.module('giffyApp')
  .controller 'MainCtrl', ($scope, $http, $rootScope, $upload) ->
    $scope.files = $scope.files || []
    $scope.delay = 100
    $scope.gif = {}

    $rootScope.$on 'new-images-dropped', ()->
      # console.log $scope.files
      $('body').addClass('loading')

    # $rootScope.$on 'new-images-read', ()->
      # console.log $scope.files
      # $('body').addClass('loading')

    $rootScope.$on 'new-gif', ()->
      # console.log $scope.files
      console.log('new gif!')
      $('body').removeClass('loading')
      $scope.$apply()

    $scope.remove = (file)->
      $scope.files.splice($scope.files.indexOf(file),1)
      $rootScope.$broadcast 'new-images-read'

    $rootScope.$on 'save-gif', ()->
      $scope.save()

    $scope.save = ()->
      $scope.upload = $upload.upload(
        url: "/api/gif"
        data: 'image'
        headers:
          'this-header': 'so-cool'
        file: $scope.gif.blob
      ).progress((evt) ->
        console.log "percent: " + parseInt(100.0 * evt.loaded / evt.total)
      ).success((data, status, headers, config) ->
        # file is uploaded successfully
        console.log data
        $rootScope.$broadcast 'saved-gif', data
      )
