'use strict'

angular.module('giffyApp')
  .config(($provide, $compileProvider, $filterProvider)->
    $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|blob):|data:image\//);
    $compileProvider.directive('gif', ['$rootScope', '$q', ($rootScope, $q) ->
      restrict: 'A'
      link: (scope, element, attrs, $compileProvider) ->
        scope.files = scope.files || []



        $rootScope.$on 'new-images-read', ()->
          # new gif maker
          gif = new GIF(
            workers: 10
            quality: 10
            workerScript: '/bower_components/gif.js/dist/gif.worker.js'
          )
          # promises for filters
          promises = []

          # for every file, render a new effect
          _.each scope.files, (file, i, files)->
            deferred = $q.defer()

            # new image element for filter and gifmaker
            img = new Image()
            img.src = file.dataURL

            # test filter for now
            effect = vintagePresets.greenish

            # our on stop callback
            options =
              onStop: ()->
                # resolve our promise
                deferred.resolve()
                # add frame to gifmaker
                gif.addFrame(img,
                  delay: scope.delay
                )

            filterd = new VintageJS(img, options, effect)
            promises.push(deferred.promise)

          $q.all(promises).then ()->
            gif.render()
            console.log 'render'

          gif.on 'finished', (blob)->
            scope.gif.blob = blob
            scope.gif.dataURL = URL.createObjectURL(blob)
            scope.$apply()
            $rootScope.$broadcast 'new-gif'
    ])
  )
