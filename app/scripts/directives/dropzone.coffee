'use strict'

angular.module('giffyApp')
  .directive('dropZone', ($q, $rootScope) ->
    restrict: 'A'
    link: (scope, element, attrs) ->

      scope.files = scope.files || []

      onDragOver = (e) ->
        e.preventDefault()
        console.log 'DRAGGING OVER'
        $("body").addClass "dragging"
        return

      onDragEnd = (e) ->
        e.preventDefault()
        # console.log 'DRAGGING ENDED', e
        $("body").removeClass "dragging"
        return

      loadFiles = (files) ->
        promises = []
        _.each files, (file, i)->

          deferred = $q.defer()

          reader = new FileReader()
          reader.onload = (e)->
            file['dataURL'] = e.target.result
            scope.files.push file
            deferred.resolve()
            scope.$apply()

          promises.push(deferred.promise)


          # console.log reader
          reader.readAsDataURL(file)


        $q.all(promises).then ()->
          console.log 'Files Read'
          $rootScope.$broadcast 'new-images-read'

      #Dragging begins on the document (shows the overlay)
      $(document).bind "dragover", onDragOver

      #Dragging ends on the overlay, which takes the whole window
      $(element).find('.drop-overlay').bind("dragleave", onDragEnd).bind "drop", (e) ->
        onDragEnd e
        $rootScope.$broadcast 'new-images-dropped'
        files = e.originalEvent.dataTransfer.files
        imageFiles = []
        _.each files, (file, i)->
          if !file.type.match 'image.*'
            alert 'SELECT IMAGES PRICK'
          else
            imageFiles.push(file)

        loadFiles imageFiles
      return



  )
