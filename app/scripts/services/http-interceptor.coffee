'use strict'

# angular.module('giffyApp')
#   .factory 'httpInterceptor', () ->
#     # Service logic
#     # ...

#     meaningOfLife = 42

#     # Public API here
#     {
#       someMethod: () ->
#         meaningOfLife
#     }


# register the interceptor as a service, intercepts ALL angular ajax http calls
angular.module("sharedServices", []).config(['$httpProvider', ($httpProvider) ->
  $httpProvider.responseInterceptors.push "myHttpInterceptor"
  spinnerFunction = (data, headersGetter) ->
    $('body').addClass 'loading'
    data

  $httpProvider.defaults.transformRequest.push spinnerFunction
]).factory "myHttpInterceptor", ['$q', '$window', ($q, $window) ->
  (promise) ->
    promise.then ((response) ->

      # do something on success
      # todo hide the spinner
      # alert "stop spinner"
      $('body').removeClass 'loading'
      response
    ), (response) ->

      # do something on error
      # todo hide the spinner
      $('body').removeClass 'loading'
      $q.reject response
]