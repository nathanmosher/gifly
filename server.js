'use strict';
global.secrets = require('./secrets.js');

// Module dependencies.
var express = require('express'),
    path = require('path'),
    fs = require('fs'),
    stylus = require('stylus'),
    nib = require('nib');

var app = express();

// Connect to database
var db = require('./lib/db/mongo');

// Bootstrap models
var modelsPath = path.join(__dirname, 'lib/models');
fs.readdirSync(modelsPath).forEach(function (file) {
  require(modelsPath + '/' + file);
});

// Populate empty DB with dummy data
require('./lib/db/dummydata');


// Express Configuration
require('./lib/config/express')(app);

// Controllers
var api = require('./lib/controllers/api'),
    index = require('./lib/controllers');

// Server Routes
app.get('/api/awesomeThings', api.awesomeThings);
app.get('/api/images', api.images);
app.post('/api/upload', api.upload);
app.post('/api/gif', api.gif);
app.get('/api/gifs', api.gifs);
app.delete('/api/gifs/all', api.purgeGifs);
app.delete('/api/images/all', api.purge);


// Angular Routes
app.get('/partials/*', index.partials);
app.get('/*', index.index);

// Start server
var port = process.env.PORT || 3000;
app.listen(port, function () {
  console.log('Express server listening on port %d in %s mode', port, app.get('env'));
});

// Expose app
exports = module.exports = app;
