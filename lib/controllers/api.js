'use strict';

var mongoose = require('mongoose'),
    Thing = mongoose.model('Thing'),
    Image = mongoose.model('Image'),
    Gif = mongoose.model('Gif'),
    async = require('async'),
    path = require('path'),
    AWS = require('aws-sdk'),
    crypto = require('crypto'),
    _ = require('underscore'),
    fs = require('fs');

AWS.config.update(global.secrets.aws);

var bucket = {};
bucket.name = 'nm-gifly';
bucket.directory = (process.env.USER == 'nmosher') ? 'beta' : '';
bucket.url = path.join('https://s3.amazonaws.com/', bucket.name);

exports.awesomeThings = function(req, res) {
  return Thing.find(function (err, things) {
    if (!err) {
      return res.json(things);
    } else {
      return res.send(err);
    }
  });
};

exports.images = function(req, res) {
  return Image.find(function (err, images) {
    if (!err) {
      return res.json(images);
    } else {
      return res.send(err);
    }
  });
};

exports.gifs = function(req, res) {
  return Gif.find(function (err, images) {
    if (!err) {
      return res.json(images);
    } else {
      return res.send(err);
    }
  });
};

exports.purge = function(req, res) {
  return Image.find({}).remove(function(){
    res.json({
      message: 'Database Cleared'
    });
  });
};

exports.purgeGifs = function(req, res) {
  return Gif.find({}).remove(function(){
    res.json({
      message: 'Database Cleared'
    });
  });
};


exports.upload = function(req, res) {
  var image = req.files.image;
  image.timestamp = new Date().getTime();
  image.hash = crypto
                .createHash( 'md5' )
                .update( image.originalFilename )
                .digest( 'hex' );
  image.ext = _.last( image.originalFilename.split('.') );
  image.newFilename = image.hash + '_' + image.timestamp + '.' + image.ext;
  image.key = path.join( bucket.directory, image.newFilename );
  image.url = path.join( bucket.url, image.key );


  fs.readFile( image.path, function (err, data) {

    var s3 = new AWS.S3();
    // send to s3
    s3.putObject({
      Bucket: 'hollye',
      Key: image.key,
      Body: data,
      ACL: 'public-read',
      ContentType: image.type
    }, function(err, data) {

      if (err) {

        console.log('Couldnt Connect to AWS: ', err);
        return res.send({ success: false, err: err });

      } else {

        console.log('Successfully submitted to AWS');

        // yay, lets update our db
        Image.create({
            originalFilename: image.originalFilename,
            url: image.url,
            created: image.timestamp,
            modified: image.timestamp,
            awsKey: image.key,
        },
        function (err, image) {
          if (!err) {
            console.log(arguments)
            return res.send({ success: true , image: image });
          } else {
            console.log('There was an error:', err)
            return res.send({ success: false , error: err });
          }
        });
      }
    });
  });
  // console.log(req.files);

  // res.send(req.files);
};


exports.gif = function(req, res) {
  var gif = req.files.file;
  gif.timestamp = new Date().getTime();
  gif.hash = crypto
                .createHash( 'md5' )
                .update( gif.originalFilename )
                .digest( 'hex' );
  gif.ext = 'gif';
  gif.newFilename = gif.hash + '_' + gif.timestamp + '.' + gif.ext;
  gif.key = path.join( bucket.directory, gif.newFilename );
  gif.url = path.join( bucket.url, gif.key );


  fs.readFile( gif.path, function (err, data) {

    var s3 = new AWS.S3();
    // send to s3
    s3.putObject({
      Bucket: bucket.name,
      Key: gif.key,
      Body: data,
      ACL: 'public-read',
      ContentType: gif.type
    }, function(err, data) {

      if (err) {

        console.log('Couldnt Connect to AWS: ', err);
        return res.send({ success: false, err: err });

      } else {

        console.log('Successfully submitted to AWS');

        // yay, lets update our db
        Gif.create({
            originalFilename: gif.originalFilename,
            url: gif.url,
            created: gif.timestamp,
            modified: gif.timestamp,
            awsKey: gif.key,
        },
        function (err, image) {
          if (!err) {
            console.log(arguments)
            return res.send({ success: true , gif: gif });
          } else {
            console.log('There was an error:', err)
            return res.send({ success: false , error: err });
          }
        });
      }
    });
  });
  // console.log(req.files);

  // res.send(req.files);
};