
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
// Schema
var GifSchema = new Schema({
  originalFilename: String,
  url: String,
  created: Number,
  modified: Number,
  awsKey: String,
});

// Validations
// ImageSchema.path('awesomeness').validate(function (num) {
//   return num >= 1 && num <= 10;
// }, 'Awesomeness must be between 1 and 10');

mongoose.model('Gif', GifSchema);
